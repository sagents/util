prettyjson = require 'prettyjson'

module.exports = ( options ) ->

  @add 'role:util,action:sanitize', ( msg, respond ) ->
    { __steps, __body } = msg

    @act 'role:transaction,action:success', {
      __steps
      ...__body
    }, respond

  @add 'role:util,action:rename', ( msg, respond ) ->
    { __rename } = msg

    Object.keys __rename
      .forEach ( key ) ->
        val = __rename[ key ]
        msg[ val ] = msg[ key ]
        delete msg[ key ]

    delete msg.__rename

    @act 'role:transaction,action:success', msg, respond

  @add 'role:util,action:log', ( msg, respond ) ->
    { keyList = [] } = msg
    data = {}

    if keyList.length is 0
      data = { ...msg }

    else

      keyList
        .forEach ( key ) ->
          data[ key ] = msg[ key ]

    console.log '=============================='
    console.log prettyjson.render data

    @act 'role:transaction,action:success', {
      ...msg
    }, respond
